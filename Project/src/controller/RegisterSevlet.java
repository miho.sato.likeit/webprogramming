package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class RegisterSevlet
 */
@WebServlet("/RegisterSevlet")
public class RegisterSevlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RegisterSevlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/uRegister.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// リクエストパラメータの文字コードを指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String loginId = request.getParameter("id");
		String password = request.getParameter("password");
		String passwordCheck = request.getParameter("passwordcheck");
		String name = request.getParameter("name");
		String birth = request.getParameter("birth");

		// Daoインスタンス
		UserDao userDao = new UserDao();

		//パスワードと確認パスワードの入力が異なる時
		if (!password.equals(passwordCheck)) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("regMsg", "新規登録に失敗しました。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/uRegister.jsp");
			dispatcher.forward(request, response);
			return;
		}

		// 入力欄何かひとつだったとき
		if(loginId.equals("")|| password.equals("")|| passwordCheck.equals("")|| name.equals("")|| birth.equals("") ) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("regMsg", "新規登録に失敗しました。");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/uRegister.jsp");
			dispatcher.forward(request, response);
			return;

		}


		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		User user = userDao.findByLoginId(loginId);

		// 既に登録されているログインIDが入力されたとき
		if (user != null) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("regMsg", "新規登録ログインに失敗しました。");

			// 新規登録jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/uRegister.jsp");
			dispatcher.forward(request, response);
			return;
		}


		// リクエストパラメータの入力項目を引数に渡して、Daoのメソッドを実行
		userDao.insertUser(loginId, password, name, birth);
		response.sendRedirect("UserListServlet");
	}

}
