package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UpdateServlet
 */
@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		//
		String id = request.getParameter("id");

		// ユーザ一覧情報を取得
		UserDao userDao = new UserDao();
		User user = userDao.findById(id);



		// リクエストスコープにデータを保存
		request.setAttribute("user", user);



		//フォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/uUpdate.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@SuppressWarnings("unused")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
        // リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String loginId = request.getParameter("loginid");
		String password = request.getParameter("password");
		String passwordCheck = request.getParameter("passwordcheck");
		String name = request.getParameter("name");
		String birth = request.getParameter("birth");

		//パスワードと確認パスワードの入力が異なる時
		if (!password.equals(passwordCheck)) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("udMsg", "入力された内容は正しくありません");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/uUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		// 入力欄何かひとつだったとき
		if(loginId.equals("")|| name.equals("")|| birth.equals("") ) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("udMsg", "入力された内容は正しくありません");

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/uUpdate.jsp");
			dispatcher.forward(request, response);
			return;

		}
		UserDao userDao = new UserDao();
		if(password.equals("")|| passwordCheck.equals("")) {


			userDao.updateUser(loginId,password,name,birth);

			response.sendRedirect("UserListServlet");
			return;
		}

		userDao.updateUser(loginId, password,name,birth);

		response.sendRedirect("UserListServlet");

	}

}
