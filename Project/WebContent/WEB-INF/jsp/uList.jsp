<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <title>ログイン画面</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"	  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <style type="text/css">
            .touroku{
                margin-left:500px;
            }
            .kennsaku{
                margin-left:500px;
            }

        </style>
    </head>
    <body>
        <form method="post" action="">
        <div class="">
            <div class="col-sm-6 mx-auto">
                <nav class="navbar navbar-light bg-light">
                  <span class="navbar-text">
                   ユーザー名表示
                  </span>
                </nav>
                <h1 class="col-sm-6 mx-auto">ユーザー覧</h1>
                <br>
                <a href="RegisterSevlet" class="touroku">新規登録</a>


                    <p>ログインID　<input type="text" name="id"></p>
                    <p>ユーザ名　<input type="text" name="name"></p>
                    <p>生年月日　<input type="date" name="birth" value="2012-02-16">　～　<input type="date" name="example" value="2012-02-16"></p>
                    <p class="kennsaku"><input type="submit" value="検索"></p>

                <table border="1" class="table">
                    <tr>
                        <th>ログインID</th>
                        <th>ユーザー名</th>
                        <th>生年月日</th>
                        <th></th>
                    </tr>
                 <c:forEach var="user" items="${userList}" >
                    <tr>
                        <td>${user.loginId}</td>
                        <td>${user.name}</td>
                        <td>${user.birthDate}</td>
                        <!-- TODO 未実装；ログインボタンの表示制御を行う -->
	                     <td>
	                       <a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
	                       <a class="btn btn-success" href="UpdateServlet?id=${user.id}">更新</a>
	                       <a class="btn btn-danger" href ="DeleteServlet?id=${user.id}">削除</a>
	                     </td>
                    </tr>
                  </c:forEach>
                </table>

            </div>
            </div>
        </form>
    </body>
</html>