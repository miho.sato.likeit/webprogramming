<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>title</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"	  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <style type="text/css">

        </style>
    </head>
    <body>
        <form method="post" action="UserDetailServlet">
        <div class="">
            <div class="col-sm-6 mx-auto">

                <nav class="navbar navbar-light bg-light">
                  <span class="navbar-text">
                   ユーザー名さん　ログアウト
                  </span>
                </nav>
                <h1 class="col-sm-8 mx-auto">ユーザ情報詳細参照</h1>
                <br>
                <div class="col-sm-8 mx-auto">
                    <p>ログインID　${user.loginId}</p>
                    <p>ユーザー名　${user.name}</p>
                    <p>生年月日　${user.birthDate}</p>
                    <p>登録日時　${user.createDate}</p>
                    <p>更新日時　${user.updateDate}</p>
                </div>
                <a href="uRegister.html">戻る</a>
            </div>
        </div>
        </form>
    </body>
</html>