<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>title</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"	  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <style type="text/css">
        </style>
    </head>
    <body>
        <form method="post" action="DeleteServlet">
            <div class="">
                <div class="col-sm-6 mx-auto">
                    <nav class="navbar navbar-light bg-light">
                        <span class="navbar-text">
                        ユーザー名さん　ログアウト
                        </span>
                    </nav>
                    <h1 class="col-sm-6 mx-auto">ユーザ削除確認</h1>
                    <br>
                    <p>
                        ログインID　: <input type="text" name="loginid" value="${user.loginId}" readonly="readonly" ><br>
                        を本当に削除してもよろしいでしょうか。
                    </p>
                    <p class="col-sm-6 mx-auto">
                        <input type="submit" value="キャンセル">
                        <input type="submit" value="OK">
                    </p>
                </div>
            </div>
        </form>
    </body>
</html>