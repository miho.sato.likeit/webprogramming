<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="ja">
    <head>
        <meta charset="UTF-8">
        <title>ログイン画面</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"	  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
         <style type="text/css">
        .test{
            text-align:  center;
        }
        </style>
    </head>
    <body>

        <form method="post" action="LoginServlet">
            <div class="test row">
                <div class="col-sm-6 mx-auto">

				<c:if test="${errMsg != null}" >
				    <div class="alert alert-danger" role="alert">
					  ${errMsg}
					</div>
				</c:if>


                <h1>ログイン画面</h1>
                <p>ログインID　<input type="text" name="id"></p>
                <p>パスワード　<input type="text" name="password"></p>
                    <p><input type="submit" value="ログイン" value="${user.message}"></p>
                </div>
            </div>
        </form>
    </body>
</html>