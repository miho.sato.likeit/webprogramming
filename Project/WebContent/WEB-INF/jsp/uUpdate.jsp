<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>title</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"	  integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <style type="text/css">
             .kousinn{
                text-align:  center;
            }
        </style>
    </head>
    <body>
        <form method="post" action="UpdateServlet">
            <div class="">
                <div class="col-sm-6 mx-auto">
                <nav class="navbar navbar-light bg-light">
                  <span class="navbar-text">
                   ユーザー名さん　ログアウト
                  </span>
                </nav>
                <h1 class="col-sm-6 mx-auto">ユーザ情報更新</h1>
				<c:if test="${udMsg != null}" >
				    <div class="alert alert-danger" role="alert">
					  ${udMsg}
					</div>
				</c:if>

                <br>
                <div class="col-sm-8 mx-auto">
                    <p>ログインID　<input type="text" name="loginid" value="${user.loginId}" readonly="readonly"></p>
                    <p>パスワード　<input type="text" name="password"></p>
                    <p>パスワード(確認)　<input type="text" name="passwordcheck"></p>
                    <p>ユーザ名　<input type="text" name="name" value="${user.name}"></p>
                    <p>生年月日　<input type="text" name="birth" value="${user.birthDate}"></p>
                </div>
                    <p class="kousinn"><input type="submit" value="更新"></p>
                <a href="uRegister.html">戻る</a>
                </div>
            </div>
        </form>
    </body>
</html>