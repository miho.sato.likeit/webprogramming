CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;

USE usermanagement;

CREATE TABLE user( 
  id SERIAL PRIMARY KEY
  , login_id varchar(255) UNIQUE
  , name varchar(255) Not Null
  ,birth_date DATE Not Null
  ,password varchar(255) Not Null
  ,create_date DATETIME Not Null
  ,update_date DATETIME Not Null
);

INSERT INTO user VALUES(
    2,'wada','wada','1996-11-26','wasabi',now(),now()
);

INSERT INTO user(login_id,name,birth_date,password,create_date,update_date) VALUES(?,?,?,?,now(),now());

SELECT * FROM user;

DELETE FROM user WHERE login_id = ?;
